const gulp = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const concat = require("gulp-concat");
const watch = require("browser-sync");
const browserSync = require("browser-sync").create();
const htmlMin = require("gulp-htmlmin");
const cssmin = require("gulp-cssmin");
const rename = require("gulp-rename");

const html = () => {
  return gulp
    .src("./src/*.html")
    .pipe(htmlMin({ collapseWhitespace: true }))
    .pipe(gulp.dest("./dist"));
};

const image = () => {
  return gulp.src("./src/image/**/*.*").pipe(gulp.dest("./dist/image"));
};

const scss = () => {
  return gulp
    .src("./src/style/**/*.{css,scss}")
    .pipe(sass.sync().on("error", sass.logError))
    .pipe(concat("style.css"))
    .pipe(cssmin())
    .pipe(rename({ suffix: ".min" }))
    .pipe(gulp.dest("./dist/style/"));
};

const watcher = () => {
  gulp.watch("./src/**/*.html", html).on("all", browserSync.reload);
  gulp.watch("./src/image/**/*.*", image).on("all", browserSync.reload);
  gulp
    .watch("./src/style/**/*.{scss,sass}", scss)
    .on("all", browserSync.reload);
};
const server = () => {
  browserSync.init({
    server: {
      baseDir: "./dist",
    },
  });
};

gulp.task("dev2", gulp.parallel(html, image,scss, server, watcher));
